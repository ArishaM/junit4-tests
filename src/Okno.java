import javax.swing.*; // Стандартная библиотека import javax.swing.event.DocumentEvent; // Отлов изменений в JTextArea
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener; // Отлов изменений в JTextArea

public class Okno extends JFrame { // Класс с импортированным JFrame

    static String text; // Это для общего использования во всей программе
    static int SummChar = 0;

    public static void main(String[] args) { // Главный метод
        Okno mainOkno = new Okno(); // Создаем приложение
        mainOkno.setTitle("Luba Test");
        mainOkno.setSize(600, 300); // Размер приложения

        JTextArea area = new JTextArea(15, 10); // Создаем поле куда будем вводить символы
        area.setText("");// Оно должно быть пустым

        text = area.getText(); // Вывод текста из поля

        // Параметры переноса слов
        area.setLineWrap(true);
        area.setWrapStyleWord(true);


        JLabel output = new JLabel();// Создаение Лейбла для вывода суммы кодов
        output.setText("Сумма кодов введенного текста равна: " + summ());// Тут понятно

        JPanel contents = new JPanel(); // создаем панель на которую помещаем поле для ввода и лейбл
        contents.add(new JScrollPane(area));
        contents.add(output);
        mainOkno.setContentPane(contents); // Устанавливаем в приложение панель как основную

        mainOkno.setResizable(false); // Изменение размеров окна вручную

        mainOkno.setTitle("Программа которая что то делает"); // Имя программы
        mainOkno.setVisible(true); // Видимость ее

        mainOkno.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // Выход при закрытии

        area.getDocument().addDocumentListener(new DocumentListener() { // Считыватель для JTextField(area)
            public void changedUpdate(DocumentEvent e) {

            }

            public void removeUpdate(DocumentEvent e) {
                text = area.getText();
                summ();
                output.setText("Сумма кодов введенного текста равна: " + summ());
            }

            public void insertUpdate(DocumentEvent e) {
                text = area.getText();
                summ();
                output.setText("Сумма кодов введенного текста равна: " + summ());
            }
        });
    }

    static int summ() { // Тут расчитываем сумму кодов символов

        for (int i = 0; i < text.length(); i++) { // Выполнять пока i < Длинны текста введенного в JTextTable
            SummChar += text.charAt(i);
        }

        return SummChar; //Возвращаем
    }

}
