import org.junit.Test;

import javax.swing.*;

import static org.junit.Assert.*;

public class OknoTest {
    @Test
    public void checkLogic1() {
        Okno.SummChar = 10;
        Okno.text = "A";

        assertEquals("Сумма сивловолов не равна нужному значению",75,Okno.summ());
    }
    @Test
    public void checkLogic2() {
        Okno.SummChar = 10;
        Okno.text = "Hello";

        assertEquals("Сумма сивловолов не равна нужному значению",510,Okno.summ());
    }
    @Test
    public void checkLogic3() {
        Okno.SummChar = 10;
        Okno.text = "Этот текст был write разными мовамi";

        assertEquals("Сумма сивловолов не равна нужному значению",26835,Okno.summ());
    }
}